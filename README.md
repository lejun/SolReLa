# Solrela

A french-translated mirror of the still online [Solrela translator](https://git.sr.ht/~rabbits/solrela), browse the [live version](https://lejun.codeberg.page/SolReLa).

- One-man translated from translations of François Sudre's original definitions by Garrison Osteen, then gradually modified and edited by the Solresol Community
- In memory of Daniel Parson(1989-2023) who built a wonderful community around Solresol.

